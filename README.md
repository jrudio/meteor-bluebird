# Bluebird for Meteor
### Version
3.2.1 (2016-02-01)

Add the latest version of Bluebird (performant promise library) to your Meteor app (server and client). Please refer to http://bluebirdjs.com for additional information and documentation.

#### How to install
```bash
meteor add jrudio:bluebird
```

Remember that Bluebird Promises is available as P (alias for Promise).
For example
```js
return new P(function (resolve, reject) {
  ...
});
```

Eventually it is possible to override the default Promise with an assignment like:
```
Promise = P;
```
Place a file inside `/lib` folder, that is the first to be loaded on Meteor.
